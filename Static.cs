﻿using System.Runtime.InteropServices;

namespace Uwabami
{
    public static class Static
    {
        public static string CockatricePath;
        public static string CockatriceDataPath;
        public static string UwabamiDataPath;
        public static string CockatriceCardsPath;
        public static string CockatriceTokensPath;
        public static class Magic
        {
            public static string CardPath = UwabamiDataPath + Path.DirectorySeparatorChar + "backup" + Path.DirectorySeparatorChar + "mtg.cards.xml";
            public static string TokenPath = UwabamiDataPath + Path.DirectorySeparatorChar + "backup" + Path.DirectorySeparatorChar + "mtg.tokens.xml";
            public static void init()
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {

                }
            }
        }
        public static class UVS
        {
            public static string CardPath;
            public static string TokenPath;
            public static void init()
            {
                CardPath = UwabamiDataPath + Path.DirectorySeparatorChar + "backup" + Path.DirectorySeparatorChar + "uvs.cards.xml";
                TokenPath = UwabamiDataPath + Path.DirectorySeparatorChar + "backup" + Path.DirectorySeparatorChar + "uvs.tokens.xml";
            }
        }

        public static void init()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                CockatricePath = "/usr/bin/cockatrice";
                CockatriceDataPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "/.local/share/Cockatrice/Cockatrice";
            }
            else
            {
                var AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                CockatricePath = "C:\\Program Files\\Cockatrice\\cockatrice.exe";
                CockatriceDataPath = $"{AppDataPath}\\Cockatrice\\Cockatrice";
            }
            UwabamiDataPath = $"{CockatriceDataPath}{Path.DirectorySeparatorChar}Uwabami";
            CockatriceCardsPath = $"{CockatriceDataPath}{Path.DirectorySeparatorChar}cards.xml";
            CockatriceTokensPath = $"{CockatriceDataPath}{Path.DirectorySeparatorChar}tokens.xml";
            UVS.init();
            Magic.init();
        }
        public static async Task UpdateUVS()
        {
            var backupDir = Directory.CreateDirectory(UwabamiDataPath + Path.DirectorySeparatorChar + "backup");
            using (var client = new HttpClient())
            {
                using (var s = await client.GetStreamAsync("https://gitlab.com/ChristopherJTrent/cockatrice_universus/-/raw/master/01.mha.xml"))
                {
                    try
                    {
                        using (var fs = new FileStream(backupDir.FullName + Path.DirectorySeparatorChar + "uvs.cards.xml", FileMode.Create))
                        {
                            s.CopyTo(fs);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e); Console.WriteLine("wtf");
                    }
                }
            }
            File.Create(UVS.TokenPath).Dispose();
        }
    }
}
