﻿// See https://aka.ms/new-console-template for more information
using System.ComponentModel;
using System.Diagnostics;
using Uwabami;
Static.init();
if (!Directory.Exists(Static.UwabamiDataPath))
{
    Console.WriteLine("Performing first-time setup. Please wait.");
    await Static.UpdateUVS();
}

if (!(File.Exists(Static.CockatriceTokensPath) && File.Exists(Static.CockatriceTokensPath)))
{
    Console.WriteLine("setup failed. Ensure that Cockatrice is installed and insure the following:");
    Console.WriteLine("1) You have run it at least once");
    Console.WriteLine("2) You have completed the first-time setup for cockatrice including downloading MTG sets.");
    return;
}

Console.WriteLine("Updating MTG Backup");
var activeCardsFile = new FileInfo(Static.CockatriceCardsPath);
var backupCardsFile = new FileInfo(Static.Magic.CardPath);
if (activeCardsFile.Exists)
{
    if ((!backupCardsFile.Exists) || (activeCardsFile.Length > backupCardsFile.Length))
    {
        File.Copy(Static.CockatriceCardsPath, Static.Magic.CardPath, true);
    } else if (activeCardsFile.Length < backupCardsFile.Length)
    {
        Console.WriteLine("Last run crash detected. Skipping card database backup and launching in UVS mode.");
    }
    else
    {
        Console.WriteLine("Card database backup is current. Skipping...");
    }
}
var activeTokensFile = new FileInfo(Static.CockatriceTokensPath);
var backupTokensFile = new FileInfo(Static.Magic.TokenPath);
if (activeTokensFile.Exists)
{
    if ((!backupTokensFile.Exists) || (activeTokensFile.Length != backupTokensFile.Length))
    {
        File.Copy(Static.CockatriceTokensPath, Static.Magic.TokenPath, true);
    }
    else if (activeTokensFile.Length < backupTokensFile.Length)
    {
        Console.WriteLine("Last run crash detected. Skipping token database backup and launching in UVS mode.");
    }
    else
    {
        Console.WriteLine("Token database backup is current. Skipping...");
    }
}
Console.WriteLine("Updating UVS Definitions");
await Static.UpdateUVS();

Console.WriteLine("Activating UVS mode");
File.Copy(Static.UVS.CardPath, Static.CockatriceCardsPath, true);
File.Copy(Static.UVS.TokenPath, Static.CockatriceTokensPath, true);

using (var P = new Process())
{
    try
    {
        P.StartInfo.UseShellExecute = true;
        P.StartInfo.CreateNoWindow = false;
        P.StartInfo.FileName = Static.CockatricePath;
        P.Start();
        P.WaitForExit();

    }
    catch (Win32Exception e)
    {
        Console.WriteLine($"Error: {e.Message}");
        Console.WriteLine("Reverting changes.");
    }
}


Console.WriteLine("Disabling UVS Mode, Enabling Magic: the Gathering mode");
File.Copy(Static.Magic.CardPath, Static.CockatriceDataPath + Path.DirectorySeparatorChar + "cards.xml", true);
File.Copy(Static.Magic.TokenPath, Static.CockatriceDataPath + Path.DirectorySeparatorChar + "Tokens.xml", true);